import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';

class CounrtyDetail extends StatelessWidget {
  final Map country;
  CounrtyDetail(this.country);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${country['name']} Detail"),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: GridView(
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            children: [
              FlipCard(
                front: BuildCard("Capital"),
                back: BuildBackCard(country["capital"], Colors.deepPurple),
                direction: FlipDirection.VERTICAL,
              ),
              FlipCard(
                front: BuildCard("Population"),
                back: BuildBackCard(
                    country["population"].toString(), Colors.deepOrange),
                direction: FlipDirection.HORIZONTAL,
              ),
              FlipCard(
                front: BuildCard("Currency"),
                back: BuildBackCard(
                    country["currencies"][0]["name"], Colors.blue),
                direction: FlipDirection.VERTICAL,
              ),
              FlipCard(
                front: BuildCard("nativeName"),
                back: BuildBackCard(country["nativeName"], Colors.cyan),
                direction: FlipDirection.HORIZONTAL,
              ),
              FlipCard(
                front: BuildCard("callingCodes"),
                back: BuildBackCard(country["callingCodes"][0], Colors.red),
                direction: FlipDirection.HORIZONTAL,
              ),
              FlipCard(
                front: BuildCard("timezones"),
                back: BuildBackCard(country["timezones"][0], Colors.pink),
                direction: FlipDirection.HORIZONTAL,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class BuildCard extends StatelessWidget {
  final String title;

  BuildCard(this.title);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3.0,
      child: Container(
        child: Center(
          child: Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}

class BuildBackCard extends StatelessWidget {
  final String title;
  final MaterialColor color;
  BuildBackCard(this.title, this.color);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        color: color,
        child: Center(
          child: Text(
            title,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
